set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" Plugins

" Surround features
" Now we can use
" sa{motion/textobj}{addition} to add surroundings 
" sdb or sd{deletion} to delete surroundings
" srb{addition} or sr{deletion}{addition} to replace surroundings
Plugin 'machakann/vim-sandwich'
" powerline nice and light
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" Syntax Highlight opencl
Plugin 'brgmnn/vim-opencl'
" Rust conf
Plugin 'rust-lang/rust.vim'


" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line 

" Relative line's number 
set relativenumber

" Number of spaces for each tab
set tabstop=4
set shiftwidth=4

" Tabs to spaces
set expandtab

" making BackSpace and Tab able to delete/insert 
" the right number of spaces
set softtabstop=4


" enable syntax and plugins
syntax enable
filetype plugin on

" FINDING FILES:

" Search down into subfolers
" Provides tab-completion for all file-related tasks
set path+=**

" Display all matching files when we tab complete
set wildmenu

" NOW WE CAN:
" - hit tab to :find by partial match
" - use * to make it fuzzy

" THINGS TO CONSIDER:
" - :b lets you autocomplete any buffer

" TAG JUMPING:
"
" Create the `tags` file
command MakeTags !ctags -R .

" NOW WE CAN:
" - Use ^] to jump to tag under cursor
" - Use g^] for ambiguous tags (g is a modifier)
" - Use ^t to jump back up the stack

" THINGS TO CONSIDER:
" - This doesn't help if you want a visual list of tags

" AUTOCOMPLETE:
" The good stuff is documented in |ins-completion|

" HIGHLIGHTS:
" - ^x^n for JUST this file
" - ^x^f for filenames (works with our path trick) 
" - ^x^] for tags only

" NOW WE CAN:
" - Use and to go back and forth in the suggestion list

" FILE BROWSING:
"  Tweaks for browsing
let g:netrw_banner=0		" Disable annoyng panel
let g:netrw_browse_split=4	" open in prior window
let g:netrw_altv=1		" open splits o the right
let g:netrw_liststyle=3		" tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide=',\{^\|\s\s\)\zs\.\S\+'

" NOW WE CAN:
" - :edit a folder or open a file browser
" - <CR>/v/t to open in an h-split/v-spli/tab
" - check |netrw-browse-maps| for more mappings

" vim-airline themes
let g:airline_theme='base16_monokai'

" Fixes mouse issues using Alacritty terminal
set ttymouse=sgr
