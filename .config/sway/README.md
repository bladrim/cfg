# Sway Config

Using pretty much the standard config right now, but need to fix some details like:
- [ ]  Custom application shortcuts;
- [ ] Do I need a decorator!?
- [ ] Rice it up, the stock blue looks too old;
- [ ] Adding snaps and flatpaks directories to PATH;
- [ ] Install a more aestetically pleasing app launcher than dmenu (or customize it);
