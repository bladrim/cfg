# STEP BY STEP: How to restore my awesome

For now it's quite simple.


# Programs we need

Everytime one of them ends up forgotten.

## Ubuntu

List of packages to install using the package manager ( *i.e.* `sudo apt install <the_package_name>`):

* `compton`, to have beautiful effects;
* `nitrogen`, to set wallpapers;
* `slock`, quick and no setup screenlock; 

## Install widgets

### awesome-wm-widgets <a name="awesome-wm-widgets"></a>

Basically all you need to do is clone the [awesome-wm-widgets](https://github.com/streetturtle/awesome-wm-widget.git) in the `$HOME/.config/awesome/` directory and then follow the documentation to add, edit or remove any of the widget you want.

## Install icons themes required by widgets

To work [awesome-wm-widgets](#awesome-wm-widgets) need the [arc-icon-theme](https://github.com/horst3180/arc-icon-theme#installation).

To install it you can proceed via **autotools**:
```
git clone https://github.com/horst3180/arc-icon-theme --depth 1 && cd arc-icon-theme
./autogen.sh --prefix=/usr
sudo make install
```
Or you can clone the [repo](https://github.com/horst3180/arc-icon-theme.git) locally, 
then copy the `Arc` folder to `~/icons` or to `/usr/share/icons`.





