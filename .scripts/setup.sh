#!/bin/bash

# For gargantua
setup_gargantua () {
    xrandr --output DVI-I-0 --rotate right --left-of HDMI-0
    xrandr --dpi 100
    g410-led -a ffffff
}

setup_gargantua_two_VGA () {
    xrandr --dpi 100
    xrandr --output HDMI-0 --off
    xrandr --output VGA-1-1 --rotate left --auto
    xrandr --output HDMI-0 --right-of VGA-1-1 --rotate right --auto
    g410-led -a ffffff
}
HOSTNAME="$(hostname)"
#[[ $HOSTNAME == "gargantua" ]] && setup_gargantua && echo "gargantua setup: comlpeted!"
[[ $HOSTNAME == "gargantua" ]] && setup_gargantua_two_VGA && echo "gargantua setup: comlpeted!"

